#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

# Utility functions related to:
#   github clone and pull

import logging
import os
import platform
import shutil

import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry

from .fs import pushd

LOGGER = logging.getLogger("H2D2.install.url")

# ---  Agent to be used for requests
if platform.system() == "Windows":
    AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36"
else:
    AGENT = "Mozilla/5.0 (X11; Linux i686; rv:64.0) Gecko/20100101 Firefox/64.0"
HEADERS = {'User-Agent': AGENT}

#==============================================================================
#==============================================================================
def urlContent(url):
    """
    Hide the download command in a function
    https://stackoverflow.com/questions/16694907/how-to-download-large-file-in-python-with-requests-py
    """
    LOGGER.debug("Get content of url %s", url)
    rep = requests.get(url, headers=HEADERS, allow_redirects=True)
    return rep.content

def urlDownload(url, dst=None, pth=os.curdir):
    """
    Hide the download command in a function
    https://stackoverflow.com/questions/16694907/how-to-download-large-file-in-python-with-requests-py
    """
    with pushd(pth):
        lclFile = dst if dst else url.split("/")[-1]
        LOGGER.debug("downloading url %s", url)

        # ---  New code - with retry
        # https://stackoverflow.com/questions/23267409/how-to-implement-retry-mechanism-into-python-requests-library?rq=1
        s = requests.Session()
        s.headers = HEADERS
        retries = Retry(total=3, backoff_factor=1, status_forcelist=[502, 503, 504])
        s.mount('http://', HTTPAdapter(max_retries=retries))
        rep = s.get(url, allow_redirects=True, stream=True)

        LOGGER.debug("   download returned code %s", rep.status_code)
        if rep.status_code == requests.codes.ok:
            with open(lclFile, 'wb') as f:
                shutil.copyfileobj(rep.raw, f)
        else:
            LOGGER.error("downloading %s returned code %s", url, rep.status_code)
    return rep.status_code == requests.codes.ok

def urlExist(url, pth=os.curdir):
    with pushd(pth):
        rep = requests.head(url, headers=HEADERS, allow_redirects=True)
    return rep.status_code == requests.codes.ok


if __name__ == "__main__":
    from context     import getTestContext
    from addLogLevel import addLoggingLevel
    addLoggingLevel('DUMP',  logging.DEBUG + 5)
    addLoggingLevel('TRACE', logging.DEBUG - 5)

    logHndlr = logging.StreamHandler()
    FORMAT = "%(asctime)s %(levelname)s %(message)s"
    logHndlr.setFormatter( logging.Formatter(FORMAT) )

    LOGGER.addHandler(logHndlr)
    LOGGER.setLevel(logging.DEBUG)
    LOGGER.info("unit test: %s", __file__)

    url = "http://www.google.com"
    urlExist(url)
