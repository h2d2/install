#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

# Utility functions related to:
#   git clone and pull

import logging
import os

from .call import doCallOrRaise, CalledProcessError

LOGGER = logging.getLogger("H2D2.install.git")

GITHUB_URL = 'https://github.com/h2d2-test/'
GITLAB_URL = 'https://gitlab.com/h2d2/'

def setGitHubURL(url):
    global GITHUB_URL
    GITHUB_URL = url if url.endswith('/') else (url + '/')

def setGitLabURL(url):
    global GITLAB_URL
    GITLAB_URL = url if url.endswith('/') else (url + '/')

def gitBranchExist(gitUrl, gitRep, gitBrn):
    url = gitUrl + gitRep + '.git'
    cmd = 'git ls-remote --heads'
    out = doCallOrRaise([cmd, url])
    for l in out.split('\n'):
        _, name = l.split()
        if name.startswith('refs/heads/'):
            tks = name.split('/')
            if len(tks) == 3 and tks[-1] == gitBrn:
                return True
    return False

def gitPull(gitUrl, gitRep, cwd=os.curdir):
    """
    git pull.
    Changes are stashed pushed and poped
    """
    gitPth = os.path.join(cwd, gitRep.split('/')[-1])
    cmd = 'git stash push'
    doCallOrRaise(cmd, cwd=gitPth)
    cmd = 'git pull -v --progress "origin"'
    doCallOrRaise(cmd, cwd=gitPth)
    try:
        cmd = 'git stash pop'
        doCallOrRaise(cmd, cwd=gitPth)
    except CalledProcessError as e:
        pass

def gitCloneMaster(gitUrl, gitRep, cwd=os.curdir):
    """
    git clone on master
    """
    gitPth = os.path.join(cwd, gitRep.split('/')[-1])
    if not os.path.isdir(gitPth):
        url = gitUrl + gitRep + '.git'
        cmd = 'git clone --recursive -v'
        doCallOrRaise([cmd, url], cwd=cwd)
    else:
        gitPull(gitUrl, gitRep, cwd=cwd)

def gitCloneBranch(gitUrl, gitRep, gitBrn, cwd=os.curdir, defaultToMaster=False):
    """
    git clone on branch.
    if defaultToMaster is True, and if the branch does not exist,
    then master will be retrieved.
    """
    gitPth = os.path.join(cwd, gitRep.split('/')[-1])
    if not os.path.isdir(gitPth):
        url = gitUrl + gitRep + '.git'
        doBranch = True
        if defaultToMaster:
            doBranch = gitBranchExist(gitUrl, gitRep, gitBrn)
        if doBranch:
            cmd = 'git clone -b ' + gitBrn + ' --recursive -v'
            doCallOrRaise([cmd, url], cwd=cwd)
        else:
            gitCloneMaster(gitUrl, gitRep, cwd=cwd)
    else:
        gitPull(gitUrl, gitRep, cwd=cwd)

def gitLabCloneMaster(gitRep, cwd=os.curdir):
    """
    git clone from GitLab on master
    """
    return gitCloneMaster(GITLAB_URL, gitRep, cwd=cwd)

def gitLabCloneBranch(gitRep, gitBrn, cwd=os.curdir, defaultToMaster=False):
    """
    git clone from GitHub on branch
    """
    return gitCloneBranch(GITLAB_URL, gitRep, gitBrn, cwd=cwd, defaultToMaster=defaultToMaster)

def gitHubCloneMaster(gitRep, cwd=os.curdir):
    """
    git clone from GitHub on master
    """
    return gitCloneMaster(GITHUB_URL, gitRep, cwd=cwd)

def gitHubCloneBranch(gitRep, gitBrn, cwd=os.curdir, defaultToMaster=False):
    """
    git clone from GitHub on branch
    """
    return gitCloneBranch(GITHUB_URL, gitRep, gitBrn, cwd=cwd, defaultToMaster=defaultToMaster)

if __name__ == "__main__":
    from context     import getTestContext
    from addLogLevel import addLoggingLevel
    addLoggingLevel('DUMP',  logging.DEBUG + 5)
    addLoggingLevel('TRACE', logging.DEBUG - 5)

    logHndlr = logging.StreamHandler()
    FORMAT = "%(asctime)s %(levelname)s %(message)s"
    logHndlr.setFormatter( logging.Formatter(FORMAT) )

    LOGGER.addHandler(logHndlr)
    LOGGER.setLevel(logging.DEBUG)
    LOGGER.info("unit test: %s", __file__)

    url = "h2d2.build"
    gitLabCloneMaster(url, cwd='.')
