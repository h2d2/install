#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

# Utility functions related to:
#   file system

import contextlib
import logging
import os
import re
import shutil
import stat
import tarfile
import zipfile
try:
    import _winapi
except ImportError:
    # on Linux
    pass

from .ptf import isWindows

LOGGER = logging.getLogger("H2D2.install.fs")

# ---  Syntactic sugar: Remove the need for user to import tarfile
TarError = tarfile.TarError

#==============================================================================
#==============================================================================
@contextlib.contextmanager
def pushd(new_dir):
    """
    https://stackoverflow.com/questions/6194499/pushd-through-os-system
    """
    previous_dir = os.getcwd()
    LOGGER.trace("pushd to %s" % new_dir)
    os.chdir(new_dir)
    try:
        LOGGER.trace("pop to %s" % previous_dir)
        yield
    finally:
        os.chdir(previous_dir)

def copytree(src, dst, symlinks=False, ignore=None):
    """
    The doc for shutil.copytree states:
    The destination directory, named by dst, must not already exist; it will be created as well as missing parent directories.
    The present versions from:
    https://stackoverflow.com/questions/1868714/how-do-i-copy-an-entire-directory-of-files-into-an-existing-directory-using-pyth
    has no such requirement.
    """
    if not os.path.exists(dst):
        os.makedirs(dst)
        shutil.copystat(src, dst)
    lst = os.listdir(src)
    if ignore:
        excl = ignore(src, lst)
        lst = [x for x in lst if x not in excl]
    for item in lst:
        s = os.path.join(src, item)
        d = os.path.join(dst, item)
        if symlinks and os.path.islink(s):
            if os.path.lexists(d):
                os.remove(d)
            os.symlink(os.readlink(s), d)
            try:
                st = os.lstat(s)
                mode = stat.S_IMODE(st.st_mode)
                os.lchmod(d, mode)
            except:
                pass # lchmod not available
        elif os.path.isdir(s):
            copytree(s, d, symlinks, ignore)
        else:
            shutil.copy2(s, d)

def locate(pattern, root=os.curdir):
    """
    Inspired from https://jksinton.com/articles/unpacking-python-locate-function
    pattern is a valid regex pattern.
    """
    regPtrn = re.compile(pattern)
    for path, dirs, files in os.walk(os.path.abspath(root)):
        matches = [ f for f in files if re.search(regPtrn, os.path.join(path, f)) ]
        for filename in matches:
            yield os.path.join(path, filename)

def getDownloadPath():
    """
    Returns the default downloads path for linux or windows
    https://stackoverflow.com/questions/35851281/python-finding-the-users-downloads-folder
    """
    if os.name == 'nt':
        import winreg
        sub_key = r'SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders'
        downloads_guid = '{374DE290-123F-4565-9164-39C4925E467B}'
        with winreg.OpenKey(winreg.HKEY_CURRENT_USER, sub_key) as key:
            location = winreg.QueryValueEx(key, downloads_guid)[0]
        return location
    else:
        return os.path.join(os.path.expanduser('~'), 'downloads')            
            
#==============================================================================
#==============================================================================
def createSymlink(src, tgt):
    LOGGER.trace("createSymlink from %s to %s", src, tgt)
    if isWindows():
        _winapi.CreateJunction(src, tgt)
    else:
        os.symlink(src, tgt)

def deleteSymlink(tgt):
    LOGGER.trace("deleteSymlink %s", tgt)
    if isWindows():
        os.unlink(tgt)
    else:
        os.remove(tgt)

def updateSymlink(src, tgt):
    LOGGER.trace("updateSymlink %s", tgt)
    if isSymlink(tgt) and os.path.realpath(tgt) != os.path.realpath(src):
        deleteSymlink(tgt)
    if not isSymlink(tgt):
        createSymlink(src, tgt)

def isSymlink(tgt):
    LOGGER.trace("isSymlink %s", tgt)
    if isWindows():
        # https://bugs.python.org/issue18306
        return os.path.islink(tgt) or \
               os.stat(tgt)[stat.ST_INO] != os.lstat(tgt)[stat.ST_INO]
    else:
        return os.path.islink(tgt)

def createHardlink(src, tgt):
    LOGGER.trace("createHardlink from %s to %s", src, tgt)
    os.link(src, tgt)

def deleteHardlink(tgt):
    LOGGER.trace("deleteHardlink %s", tgt)
    os.unlink(tgt)

def isHardlink(tgt):
    LOGGER.trace("isHardlink %s", tgt)
    return os.path.isfile(tgt) and os.stat(tgt)[stat.ST_NLINK] >  1

#==============================================================================
#==============================================================================
def untarFile(tarFic, path="."):
    with tarfile.open(tarFic) as fic:
        fic.extractall(path=path)

def unzipFile(zipFic):
    with zipfile.ZipFile(zipFic, 'r') as fic:
        fic.extractall()


if __name__ == "__main__":
    logHndlr = logging.StreamHandler()
    FORMAT = "%(asctime)s %(levelname)s %(message)s"
    logHndlr.setFormatter( logging.Formatter(FORMAT) )

    LOGGER.addHandler(logHndlr)
    LOGGER.setLevel(logging.DEBUG)
    LOGGER.info('unit test: %s' % __file__)

    pass
