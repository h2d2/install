::-----------------------------------------------------------------------------
:: Copyright (c) INRS 2011-2018
:: Institut National de la Recherche Scientifique (INRS)
::
:: Distributed under the GNU Lesser General Public License, Version 3.0.
:: See accompanying file LICENSE.txt.
::-----------------------------------------------------------------------------
@echo off
setlocal EnableExtensions EnableDelayedExpansion

::-----------------------------------------------------------------------------
:: ---  Configuration
:: CFG_VERS   Version
:: CFG_TOOL   Application name
:: CFG_FCFG   Name of configuration file
::-----------------------------------------------------------------------------
set CFG_VERS=19.04
set CFG_TOOL=H2D2
set CFG_FCFG=h2d2-cfg.bat
set CFG_CFIG=h2d2-configure.bat
::-----------------------------------------------------------------------------

::-----------------------------------------------------------------------------
:: h2d2-lauch takes 2 parameters;
::    H2D2_KND : the kind of commande, one of [bat, exe, cmd]
::    H2D2_CMD : the actual command. The commant can contains tokens
::               that will be substituated. Known tokens ar:
::               {H2D2_BSE_PATH}
::               {H2D2_BIN_PATH}
::               {H2D2_DOC_PATH}
::
:: h2d2_lauch will extract the H2D2 directory corresponding to the 
:: actual configuration.
::-----------------------------------------------------------------------------

:: ---  Execute main
if ["%H2D2_KND%"] EQU [""] goto err_argument
if ["%H2D2_CMD%"] EQU [""] goto err_argument
call :main %*
goto :EOF

::-----------------------------------------------------------------------------
:: Get directory for config file
::
:: Input:
::
:: Output:
::    RET_1    Config file directory
::-----------------------------------------------------------------------------
:get_config_file_dir
   setlocal EnableExtensions EnableDelayedExpansion

   set _ROOT=.
   if exist "%LocalAppData%" goto get_config_file_dir_localdata
   if exist "%AppData%"      goto get_config_file_dir_appdata
   goto err_environment

   :get_config_file_dir_appdata
   set _ROOT=%AppData%
   goto get_config_file_dir_root_done
   :get_config_file_dir_localdata
   set _ROOT=%LocalAppData%
   goto get_config_file_dir_root_done

   :get_config_file_dir_root_done
   endlocal & set RET_1=%_ROOT%\%CFG_TOOL%
goto :EOF

::-----------------------------------------------------------------------------
:: The function split_path splits the path (1st argument) in two components:
:: the filename and the root path. The scope is local.
::
:: Input:
::    %1 Path
::
:: Output:
::    RET_1    Filename
::    RET_2    Dir
::-----------------------------------------------------------------------------
:split_path
   setlocal EnableExtensions EnableDelayedExpansion
   set _NAM=%~n1
   set _DIR=%~dp1
   set _DIR=%_DIR:~0,-1%
   endlocal & set RET_1=%_NAM%& set RET_2=%_DIR%
goto :EOF

::-----------------------------------------------------------------------------
:: The function split_ext splits the path (1st argument) in two components:
:: the extension and the filename inclusive path. The scope is local.
::
:: Input:
::    %1 Path
::
:: Output:
::    RET_1    Extension
::    RET_2    Filename
::-----------------------------------------------------------------------------
:split_ext
   setlocal EnableExtensions EnableDelayedExpansion
   set _EXT=%~x1
   set _NAM=%~dpn1
   endlocal & set RET_1=%_EXT%& set RET_2=%_NAM%
goto :EOF

::-----------------------------------------------------------------------------
:: Main
::-----------------------------------------------------------------------------
:main

   :: ---  Get installation directory
   set H2D2_INST_DIR=%~f0
   call :split_path "%H2D2_INST_DIR%" & set H2D2_INST_DIR=!RET_2!
   call :split_path "%H2D2_INST_DIR%" & set H2D2_INST_DIR=!RET_2!

   :: ---  Run config file
   call :get_config_file_dir & set H2D2_CFG_FILE=!RET_1!\%CFG_FCFG%
   if not exist "%H2D2_CFG_FILE%" goto err_config
   call "%H2D2_CFG_FILE%"

   :: ---  Set PATH
   set H2D2_BSE_PATH=%H2D2_INST_DIR%\%H2D2_RUN_PATH%
   set H2D2_BIN_PATH=%H2D2_INST_DIR%\%H2D2_RUN_PATH%\bin
   set H2D2_DOC_PATH=%H2D2_INST_DIR%\%H2D2_RUN_PATH%\doc
   set H2D2_CMN_PATH=%H2D2_INST_DIR%\%H2D2_CMN_PATH%
   set H2D2_CPL_PATH=%H2D2_INST_DIR%\%H2D2_CPL_PATH%
   set PATH=%H2D2_CPL_PATH%;%H2D2_CMN_PATH%;%PATH%

   :: ---  Forme the command, tokens substitution
   set H2D2_CMD=!H2D2_CMD:{H2D2_BSE_PATH}=%H2D2_BSE_PATH%!
   set H2D2_CMD=!H2D2_CMD:{H2D2_BIN_PATH}=%H2D2_BIN_PATH%!
   set H2D2_CMD=!H2D2_CMD:{H2D2_DOC_PATH}=%H2D2_DOC_PATH%!
   set H2D2_CMD=!H2D2_CMD:\=/!
   
   :: ---  Run H2D2_EXE
   if [%H2D2_KND%] EQU [bat] goto exec_bat
   if [%H2D2_KND%] EQU [exe] goto exec_exe
   if [%H2D2_KND%] EQU [cmd] goto exec_cmd
   goto err_bad_kind
   
   :exec_bat
      if not exist "%H2D2_CMD%" goto err_no_exe
      chcp 65001 > nul
      call "%H2D2_CMD%" %*
      goto exec_done
   :exec_exe
      if not exist "%H2D2_CMD%" goto err_no_exe
      chcp 65001 > nul
      "%H2D2_CMD%" %*
      goto exec_done
   :exec_cmd
      chcp 65001 > nul
      %H2D2_CMD% %*
      goto exec_done
   :exec_done

goto :EOF

::-----------------------------------------------------------------------------
:err_argument
echo Error: H2D2_KND and H2D2_CMD environment variable must be set
echo to the script or executable to launch
echo.
pause
goto error

:err_environment
echo Error: No Application Data directory
echo Environment Variable AppData or LocalAppData must be defined
echo.
pause
goto error

:err_config
echo Error: Missing %CFG_FCFG%
echo Please run %CFG_CFIG% first
echo.
pause
goto error

:err_no_exe
echo Error: Invalid path; %H2D2_EXE% could not be located in:
echo    %H2D2_RUN_PATH%
echo Please run %CFG_CFIG% first
echo.
pause
goto error

::-----------------------------------------------------------------------------
:error
exit /b 2
