::-----------------------------------------------------------------------------
:: Copyright (c) INRS 2011-2018
:: Institut National de la Recherche Scientifique (INRS)
::
:: Distributed under the GNU Lesser General Public License, Version 3.0.
:: See accompanying file LICENSE.txt.
::-----------------------------------------------------------------------------
@echo off
setlocal EnableExtensions EnableDelayedExpansion

::-----------------------------------------------------------------------------
:: ---  Configuration
:: CFG_VERS   Version
:: CFG_TOOL   Application name
:: CFG_FCFG   Name of configuration file
:: CFG_STOK   Target exe
::-----------------------------------------------------------------------------
set CFG_VERS=21.10
set CFG_TOOL=H2D2-tools
set CFG_FCFG=h2d2-tools-cfg.bat
set CFG_STOK=DADataAnalyzer.py;DADataAnalyzer.exe
::-----------------------------------------------------------------------------

:: ---  Initialise global variables
set /a GLB_COUNT=0
:: https://stackoverflow.com/questions/21660249/how-do-i-make-one-particular-line-of-a-batch-file-a-different-color-then-the-oth
for /F "tokens=1,2 delims=#" %%a in ('"prompt #$H#$E# & echo on & for %%b in (1) do rem"') do (
  set "CLR_COD=%%a"
)

:: ---  Execute main
echo %CFG_TOOL% 
echo Configuration manager Version %CFG_VERS%
echo.
call :main %*
goto :EOF

::-----------------------------------------------------------------------------
:: The function get_val returns the value of the variable whose name
:: is passed as argument. The scope is local.
::
:: Input:
::    %1       Input path
::
:: Output:
::    RET_1    Stripped value
::
:: http://ss64.com/nt/syntax-dequote.html
::-----------------------------------------------------------------------------
:get_val
   setlocal EnableExtensions EnableDelayedExpansion
   set _VAL=%1
   call set _VAL=%%!_VAL!%%
   if "%_VAL:~0,1%"  == " " set _VAL=!_VAL:~1!
   if "%_VAL:~-1,1%" == " " set _VAL=!_VAL:~0,-1!
   endlocal & set RET_1=%_VAL%
goto :EOF

::-----------------------------------------------------------------------------
:: The function split_path splits the path (1st argument) in two components:
:: the filename and the root path. The scope is local.
::
:: Input:
::    %1 Path
::
:: Output:
::    RET_1    Filename
::    RET_2    Dir
::-----------------------------------------------------------------------------
:split_path
   setlocal EnableExtensions EnableDelayedExpansion
   set _NAM=%~nx1
   set _DIR=%~dp1
   set _DIR=%_DIR:~0,-1%
   endlocal & set RET_1=%_NAM% & set RET_2=%_DIR%
goto :EOF

::-----------------------------------------------------------------------------
:: The function split_string splits the string (1st argument) at the
:: position of the split token (2nd argument). The scope is local.
::
:: Input:
::    %1 string
::    %2 split token
::
:: Output:
::    RET_1    lhs
::    RET_2    rhs
::-----------------------------------------------------------------------------
:split_string
   setlocal EnableExtensions EnableDelayedExpansion
   SET _STR=%1
   SET _TOK=%2
   CALL SET _RHS=%%_STR:*%_TOK%=%%
   CALL SET _LHS=%%_STR:%_RHS%=%%
   set _LHS=%_LHS:~0,-1%
   endlocal & set RET_1=%_LHS% & set RET_2=%_RHS%
goto :EOF

::-----------------------------------------------------------------------------
:: The function process_one_path extract the pieces of the configuration
:: from the path (1st argument). It sets global variables in the form
:: of arrays. The scope is global because the arrays and the counter are
:: of global scope.
::
:: Input:
::    %1    Input path
::    %2    H2D2 root path
::
:: Output:
::    H2D2_RUN_PTH[nn]
::    H2D2_RUN_PTF[nn]
::-----------------------------------------------------------------------------
:process_one_path
   set _PTH=%~f1
   set _ROO=%~f2

   :: strip script/DADataAnalyzer/DADataAnalyzer.py
   call :split_path "%_PTH%" & set _PTH=!RET_2!
   call :split_path "%_PTH%" & set _PTH=!RET_2!
   call :split_path "%_PTH%" & set _PTH=!RET_2!
   call set _NAM=%%_PTH:*%_ROO%\=%%

   :: Extract version
   call :split_path "%_PTH%" & set _VER=!RET_1! & set _PTH=!RET_2!

   set /a GLB_COUNT=%GLB_COUNT%+1
   set H2D2_RUN_PTH[%GLB_COUNT%]=%_NAM: =%

   set _PTH=
   set _NAM=
goto :EOF

::-----------------------------------------------------------------------------
:: The function get_configurations will get all H2D2 configurations rooted at
:: the path (1s argument). It calls process_one_path for each path.
:: The scope is global.
::
:: Input:
::    %1    The root directory
::
::-----------------------------------------------------------------------------
:get_configurations
   pushd %1
   for /f "delims=|" %%p in ('dir /b /s %CFG_STOK%') do call :process_one_path "%%~fp" %1
   popd
goto :EOF

::-----------------------------------------------------------------------------
:: The function get_user_choice set-up the menu and returns the user choice. The
:: menu is build from the information gathered by get_configurations.
:: The scope is local.
::
:: Input:
::    None
::
:: Ouput:
::    RET_1    The index of the choice
::-----------------------------------------------------------------------------
:get_user_choice
   setlocal EnableExtensions EnableDelayedExpansion
   echo.
   echo Installed configurations
   echo ========================
   set /a i=1
   :get_user_choice_loop0
   if %i% GTR %GLB_COUNT% goto get_user_choice_endloop0
      call :get_val H2D2_RUN_PTH[%i%]
      set istr=    %i%
      if %GLB_COUNT% LSS 1000 set istr=!istr:~-3!
      if %GLB_COUNT% LSS  100 set istr=!istr:~-2!
      if %GLB_COUNT% LSS   10 set istr=!istr:~-1!
      set /a ic=%i% %% 2
      if [%ic%] EQU [1] (
         call :colorEcho 07 "[%istr%] %RET_1:\= %"
      ) else (
         call :colorEcho 06 "[%istr%] %RET_1:\= %"
      )
      set /a i+=1
   goto get_user_choice_loop0
   :get_user_choice_endloop0
   echo.
   :: Get user choice
   set /P _INDX=Choose the appropriate configuration (q to quit): 
   endlocal & set RET_1=%_INDX%
goto :EOF

:colorEcho
   setlocal EnableExtensions EnableDelayedExpansion
   :: Coloring rely on a trickery by creating files. Not very safe!!!
   ::echo off
   ::<nul set /p ".=%CLR_COD%" > "%~2"
   ::findstr /v /a:%1 /R "^$" "%~2" nul
   ::CLR_COD "%~2" > nul 2>&1
   ::echo.
   echo %~2
   endlocal
goto :EOF

::-----------------------------------------------------------------------------
:: Get directory for config file. If the diretory does not exist, it
:: will be created.
::
:: Input:
::
:: Output:
::    RET_1    Config file directory
::-----------------------------------------------------------------------------
:get_config_file_dir
   setlocal EnableExtensions EnableDelayedExpansion
   set _ROOT=.
   if exist "%LocalAppData%" goto get_config_file_dir_localdata
   if exist "%AppData%"      goto get_config_file_dir_appdata
   echo Error: No Application Data directory
   echo Environment Variable AppData or LocalAppData must be defined
   pause

   :get_config_file_dir_appdata
   set _ROOT=%AppData%
   goto get_config_file_dir_root_done
   :get_config_file_dir_localdata
   set _ROOT=%LocalAppData%
   goto get_config_file_dir_root_done

   :get_config_file_dir_root_done
   set _ROOT=%_ROOT%\%CFG_TOOL%
   if not exist %_ROOT% mkdir %_ROOT%

   endlocal & set RET_1=%_ROOT%
goto :EOF

::-----------------------------------------------------------------------------
:: The function write_config_file write the configuration file
:: corresponding to the user choice (2nd parameter).
::
:: Input:
::    %1       Installation root directory
::    %2       Index of user choice
::
:: Output:
::    None
::-----------------------------------------------------------------------------
:write_config_file
   setlocal EnableExtensions EnableDelayedExpansion
   set _CFG_ROOT=%~1
   set _CFG_INDX=%2

   :: ---  Get run path
   call :get_val H2D2_RUN_PTH[%_CFG_INDX%] & set _RUN_PATH=!RET_1!

   :: ---  Write config file
   if ERRORLEVEL 1 goto :EOF
   set _CFG_FILE=%_CFG_ROOT%\%CFG_FCFG%
   echo Writing configuration: [%_CFG_INDX%] %_RUN_PATH:\= %
   echo to "%_CFG_FILE%"
   echo ::%CFG_TOOL% active configuration > "%_CFG_FILE%"
   echo @set H2D2_RUN_PATH=%_RUN_PATH%>> "%_CFG_FILE%"

   endlocal
goto :EOF

::-----------------------------------------------------------------------------
:: Main
::-----------------------------------------------------------------------------
:main
   setlocal EnableExtensions EnableDelayedExpansion

   :: ---  Get installation directory
   set H2D2_INST_DIR=%~f0
   call :split_path "%H2D2_INST_DIR%" & set H2D2_INST_DIR=!RET_2!
   call :split_path "%H2D2_INST_DIR%" & set H2D2_INST_DIR=!RET_2!
   
   :: ---  Process
   call :get_configurations "%H2D2_INST_DIR%"
   call :get_config_file_dir  & set H2D2_CFG_ROOT=!RET_1!
   call :get_user_choice      & set H2D2_CFG_INDX=!RET_1!
   if /I [%H2D2_CFG_INDX%] == [q] goto get_main_done
   call :write_config_file "%H2D2_CFG_ROOT%" %H2D2_CFG_INDX%

   :get_main_done
   endlocal
goto :EOF

::-----------------------------------------------------------------------------
:error
exit /b 2

