#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import fnmatch
import glob
import logging
import os
import shutil
import tarfile
import tempfile

import tools.ptf  as tl_ptf
import tools.fs   as tl_fs
import tools.call as tl_call

LOGGER = logging.getLogger("H2D2.install.H2D2")

EXCLUDE_PATTERNS = []
INCLUDE_PATTERNS = []

def tarFilter(tarinfo):
    for ptrn in EXCLUDE_PATTERNS:
        if fnmatch.fnmatch(tarinfo.name, ptrn):
            return None
            
    cmps = tarinfo.name.split('/')
    ncmps= len(cmps)
    for i, ptrn in INCLUDE_PATTERNS:
        if i < ncmps:
            if not fnmatch.fnmatch(cmps[i], ptrn):
                return None
    return tarinfo

def tarH2D2 (h2d2Dir, h2d2Ver, h2d2Mpi, h2d2Cpl):
    global EXCLUDE_PATTERNS
    global INCLUDE_PATTERNS
    LOGGER.trace(".".join([os.path.splitext(__file__)[0], "tarH2D2"]))

    INCLUDE_PATTERNS.append( (3, h2d2Cpl) ) # index must be ordered

    EXCLUDE_PATTERNS.append("*~")
    if not tl_ptf.isUnix():    EXCLUDE_PATTERNS.append("*.sh")
    if not tl_ptf.isWindows(): EXCLUDE_PATTERNS.append("*.bat")
    EXCLUDE_PATTERNS.append("*.a")
    EXCLUDE_PATTERNS.append("*.lib")
    EXCLUDE_PATTERNS.append("*.gz")
    EXCLUDE_PATTERNS.append("*.tgz")
    EXCLUDE_PATTERNS.append("*.bz2")
    EXCLUDE_PATTERNS.append("*.zip")
    EXCLUDE_PATTERNS.append("*.log")
    ## EXCLUDE_PATTERNS=$EXCLUDE_PATTERNS" --exclude=openmpi-1.6"
    ## EXCLUDE_PATTERNS=$EXCLUDE_PATTERNS" --exclude=openmpi-1.8"
    ## #EXCLUDE_PATTERNS=$EXCLUDE_PATTERNS" --exclude=openmpi-1.10"
    ## EXCLUDE_PATTERNS=$EXCLUDE_PATTERNS" --exclude=unx32"
    ## #EXCLUDE_PATTERNS=$EXCLUDE_PATTERNS" --exclude=unx64"
    ## EXCLUDE_PATTERNS=$EXCLUDE_PATTERNS" --exclude=unx64i8"
    ## EXCLUDE_PATTERNS=$EXCLUDE_PATTERNS" --exclude=sun-*"
    ## #EXCLUDE_PATTERNS=$EXCLUDE_PATTERNS" --exclude=sun-12.4"
    ## #EXCLUDE_PATTERNS=$EXCLUDE_PATTERNS" --exclude=sun-12.6"
    ## EXCLUDE_PATTERNS=$EXCLUDE_PATTERNS" --exclude=open64-*"
    ## EXCLUDE_PATTERNS=$EXCLUDE_PATTERNS" --exclude=itlX86-*"
    ## EXCLUDE_PATTERNS=$EXCLUDE_PATTERNS" --exclude=itlX64-14.0"
    ## EXCLUDE_PATTERNS=$EXCLUDE_PATTERNS" --exclude=itlX64-15.0"
    ## EXCLUDE_PATTERNS=$EXCLUDE_PATTERNS" --exclude=itlX64-16.0"
    ## EXCLUDE_PATTERNS=$EXCLUDE_PATTERNS" --exclude=itlX64-17.0"
    ## EXCLUDE_PATTERNS=$EXCLUDE_PATTERNS" --exclude=itl*/debug"
    EXCLUDE_PATTERNS.append("bin-*")
    EXCLUDE_PATTERNS.append("bin/intel")
    EXCLUDE_PATTERNS.append("bin/open64")
    EXCLUDE_PATTERNS.append("bin/sun")

    EXCLUDE_PATTERNS.append(".gitignore")
    EXCLUDE_PATTERNS.append("__pycache__")
    EXCLUDE_PATTERNS.append("*/__pycache__")

    h2d2Ptf = tl_ptf.getPlatformName()
    tarFic = "H2D2-%s_%s_%s.tar.gz" % (h2d2Ver, h2d2Ptf, h2d2Mpi)
    LOGGER.debug("tar to %s", tarFic)
    with tl_fs.pushd(h2d2Dir):
        with tarfile.open(tarFic, 'w:gz', dereference=True) as tar:
            for item in glob.glob("*"):
                tar.add(item, filter=tarFilter)

    return tarFic

def doWork(ctx):
    LOGGER.trace(".".join([os.path.splitext(__file__)[0], "doWork"]))

    # ---  Unlink all
    for item in reversed( glob.glob("**/", recursive=True) ):
        if tl_fs.isSymlink(item):
            try:
                tl_fs.deleteSymlink(item)
            except:
                LOGGER.error('Directory not clean')

    # ---  Create link
    LOGGER.debug("Create symbolic link")
    src = os.path.join(ctx.baseDir, "H2D2", "_run", "H2D2")
    if not os.path.isdir(src):
        raise RuntimeError('Invalid binary directory:\n   %s' % src)
    lnk = "H2D2-%s" % ctx.version
    tl_fs.createSymlink(src, lnk)
    ptf = tl_ptf.getPlatformName()
    src = os.path.realpath( os.path.join("..", "bin-%s" % ptf, "bin-H2D2") )
    tl_fs.createSymlink(src, "bin")

    # ---  tar version
    LOGGER.debug("tar H2D2")
    tarFic = tarH2D2(".", ctx.version, ctx.MPIlib, ctx.complr)

    # ---  Unlink all
    LOGGER.debug("Delete symbolic link")
    if tl_fs.isSymlink(lnk):   tl_fs.deleteSymlink(lnk)
    if tl_fs.isSymlink("bin"): tl_fs.deleteSymlink("bin")

    for item in glob.glob("./bin*"):
        if tl_fs.isSymlink(item):
            tl_fs.deleteSymlink(item)
    for item in glob.glob("./H2D2-*"):
        if tl_fs.isSymlink(item):
            tl_fs.deleteSymlink(item)

    # ---  Copy to ctx.optDir
    LOGGER.debug("Copy to %s", ctx.optDir)
    shutil.copy(tarFic, ctx.optDir)

    # ---  untar
    LOGGER.debug("Untar in %s", os.path.join(ctx.optDir, "H2D2"))
    with tl_fs.pushd(ctx.optDir):
        if not os.path.isdir("H2D2"):
            os.mkdir("H2D2")
        tl_fs.untarFile(tarFic, path="H2D2")

    # ---  Log
    binDir = os.path.join(ctx.optDir, "H2D2", "bin")
    if tl_ptf.isWindows():
        print()
        print("===")
        print("You may want to modify PATH in your ENVIRONMENT")
        print("   PATH=%s;%%PATH%%" % binDir)
        if not 'TMP' in os.environ:
            print("   TMP=%s" % tempfile.gettempdir())
        print("===")
        print()
    else:
        with tl_fs.pushd(ctx.optDir):
            ptrn = "%s-*-unx64*" % ctx.MPIlib
            mpiDirs = glob.glob(ptrn)
        print()
        print("===")
        print("You may want to add the following lines to your .bash_aliases")
        if not 'TMP' in os.environ:
            print("   export TMP=%s" % tempfile.gettempdir())
        if len(mpiDirs) < 1:
            print("   export PATH=%s:$PATH" % binDir)
        elif len(mpiDirs) == 1:
            mpiDir = mpiDirs[0]
            mpiBin = os.path.join(ctx.optDir, mpiDir, "bin")
            mpiLib = os.path.join(ctx.optDir, mpiDir, "lib")
            print("   export PATH=%s:%s:$PATH" % (binDir, mpiBin))
            print("   export LD_LIBRARY_PATH=%s:$LD_LIBRARY_PATH" % mpiLib)
        else:
            print("   export PATH=%s:$PATH" % (binDir))
            print()
            print("and one of the following to address the correct MPI library:")
            for mpiDir in mpiDirs:
                mpiBin = os.path.join(ctx.optDir, mpiDir, "bin")
                mpiLib = os.path.join(ctx.optDir, mpiDir, "lib")
                print("   export PATH=%s:$PATH" % mpiBin)
                print("   export LD_LIBRARY_PATH=%s:$LD_LIBRARY_PATH" % mpiLib)
                if mpiDir != mpiDirs[-1]:
                    print("or:")
        print("===")
        print()

def xeq(ctx):
    LOGGER.trace(".".join([os.path.splitext(__file__)[0], "xeq"]))

    # ---  Shortcut
    assert ctx.target, "ctx.target must be defined"
    if not ctx.isH2D2(): return

    # ---  Header
    LOGGER.info("H2D2 - package H2D2")

    # ---  Pre-conditions
    assert ctx.baseDir, "ctx.baseDir must be defined"
    assert ctx.optDir,  "ctx.optDir must be defined"
    assert ctx.version, "ctx.version must be defined"

    # ---  Create stage directory
    if not os.path.isdir("_stage"):
        os.mkdir("_stage")

    with tl_fs.pushd("_stage"):
        doWork(ctx)

    # ---  Footer
    LOGGER.info("H2D2 - package H2D2: Done")

if __name__ == "__main__":
    from tools.context     import getTestContext
    from tools.addLogLevel import addLoggingLevel
    addLoggingLevel('DUMP',  logging.DEBUG + 5)
    addLoggingLevel('TRACE', logging.DEBUG - 5)

    logHndlr = logging.StreamHandler()
    FORMAT = "%(asctime)s %(levelname)s %(message)s"
    logHndlr.setFormatter( logging.Formatter(FORMAT) )

    LOGGER = logging.getLogger("H2D2.install")
    LOGGER.addHandler(logHndlr)
    LOGGER.setLevel(logging.TRACE)
    LOGGER.info("unit test: %s", __file__)

    ctx = getTestContext()
    xeq(ctx)
