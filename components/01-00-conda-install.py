#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import logging
import os
import shutil
import stat
import sys

import tools.ptf  as tl_ptf
import tools.fs   as tl_fs
import tools.call as tl_call
import tools.url  as tl_url

LOGGER = logging.getLogger("H2D2.install.conda-install")

CONDA_PKGS = {
   'base' : [ ],
   'h2d2' : [ ],
   'tools': [["numpy"],
             ["wxpython", "matplotlib", "pytimeparse", "gdal", "pillow", "pyshp", "scipy"],
             ["-c bjornfjohansson", "mpldatacursor"]],
   }

def is_sudo():
    try:
        ret = tl_call.doCallOrRaise("sudo -v", doLog=False)
    except:
        ret = True
    return not ret

def has_conda():
    """
    https://stackoverflow.com/questions/47608532/how-to-detect-from-within-python-whether-packages-are-managed-with-conda
    """
    return os.path.exists(os.path.join(sys.prefix, 'conda-meta'))

def is_conda_system():
    pth = shutil.which("conda")
    if not pth: return False
    if tl_ptf.isWindows():
        if pth.startswith( os.environ['ProgramData'] ): return True
        if not os.access( os.path.dirname(pth), os.W_OK ): return True
        return False
    else:
        bin_dir = os.path.dirname(pth)
        bse_dir = os.path.dirname(bin_dir)
        lib_dir = os.path.join(bse_dir, 'lib')
        if not os.path.isdir(lib_dir): return True
        if not os.access(bse_dir, os.W_OK ): return True
        return False

def installUnx(ctx):
    LOGGER.trace(".".join([os.path.splitext(__file__)[0], "installUnx"]))
    # --- Miniconda
    condaVer = "Miniconda3-latest-Linux-x86_64"
    condaFil = condaVer + ".sh"
    condaUrl = "https://repo.anaconda.com/miniconda/" + condaFil
    condaDir = os.path.join(ctx.optDir, "miniconda3")
    # --- Anaconda
    # condaVer=Anaconda3-2018.12-Linux-x86_64
    # condaFil=$condaVer.sh
    # condaUrl=https://repo.continuum.io/archive/$condaFil
    # condaDir=os.path.join(ctx.optDir, "Anaconda3")

    # ---  Create opt
    if not os.path.isdir(ctx.dwnDir):
        os.mkdir(ctx.dwnDir)
    if not os.path.isdir(ctx.optDir):
        os.mkdir(ctx.optDir)

    if not has_conda():
        with tl_fs.pushd(ctx.dwnDir):
            if not os.path.isfile(condaFil):
                LOGGER.debug("Download %s", condaFil)
                isOk = tl_url.urlDownload(condaUrl)
                if not isOk:
                    LOGGER.error("Warning: Could not download Conda")
                    LOGGER.error("   URL: %s", condaUrl)
                    exit(1)

    if not has_conda():
        with tl_fs.pushd(ctx.dwnDir):
            if not os.path.isdir(condaDir):
                LOGGER.debug("Extract %s", condaFil)
                st = os.stat(condaFil)
                os.chmod(condaFil, st.st_mode | stat.S_IEXEC)
                tl_call.doCallOrRaise(["./"+ condaFil, "-b -f -p", condaDir])
                print()
                print("===")
                print("You may want to add %s to your PATH" % os.path.join(condaDir, "bin"))
                print("   export PATH=%s:$PATH" % os.path.join(condaDir, "bin"))
                print("and restart the scripts with conda python")
                print("===")
                print()
                exit(1)
                newPATH = os.path.join(condaDir, "bin")
                newPATH = ':'.join( [newPATH, os.environ["PATH"]] )
                os.putenv("PATH", newPATH)

    if has_conda():
        if is_conda_system() and not is_sudo():
            LOGGER.warning("Conda is detected as system wide install.")
            LOGGER.warning("You may want to run the following commands in an elevated conda prompt:")
            LOGGER.warning("   conda update --all")
            for pkg in CONDA_PKGS['base']:
                LOGGER.warning("   conda install %s", ' '.join(pkg))

            grp = 'h2d2' if ctx.isH2D2() else 'tools'
            for pkg in CONDA_PKGS[grp]:
                LOGGER.warning("   conda install %s", ' '.join(pkg))
        else:
            if is_conda_system():
                LOGGER.warning("Conda is detected as system wide install.")
                conda_cmd = "sudo conda"
            else:
                conda_cmd = "conda"
            LOGGER.info("Update conda")
            tl_call.doCallOrRaise([conda_cmd, "update", "--yes", "--all"])

            for pkg in CONDA_PKGS['base']:
                LOGGER.info("Install %s", pkg)
                tl_call.doCallOrRaise([conda_cmd, "install", "--yes"] + pkg)

            grp = 'h2d2' if ctx.isH2D2() else 'tools'
            for pkg in CONDA_PKGS[grp]:
                LOGGER.info("Install %s", pkg)
                tl_call.doCallOrRaise([conda_cmd, "install", "--yes"] + pkg)

    if not has_conda():
        LOGGER.critical("scripts must be lauched with conda python")
        exit(1)

def installWin(ctx):
    LOGGER.trace(".".join([os.path.splitext(__file__)[0], "installWin"]))
    # --- Miniconda
    condaVer = "Miniconda3-latest-Windows-x86_64"
    condaFil = condaVer + ".exe"
    condaUrl = "https://repo.anaconda.com/miniconda/" + condaFil
    condaDir = os.path.join(os.environ["UserProfile"], "Miniconda3")
    # --- Anaconda
    # condaVer = Anaconda3-2018.12-Linux-x86_64
    # condaFil = condaVer + ".exe"
    # condaUrl = "https://repo.continuum.io/archive/" + condaFil
    # condaDir = os.path.join(os.environ["UserProfile"], "Anaconda3")

    # ---  Create opt
    if not os.path.isdir(ctx.optDir):
        os.mkdir(ctx.optDir)

    if not has_conda():
        with tl_fs.pushd(ctx.dwnDir):
            if not os.path.isfile(condaFil):
                LOGGER.debug("Download %s", condaFil)
                isOk = tl_url.urlDownload(condaUrl)
                if not isOk:
                    LOGGER.error("Warning: Could not download Conda")
                    LOGGER.error("   URL: %s", condaUrl)
                    exit(1)

    if not has_conda():
        with tl_fs.pushd(ctx.dwnDir):
            # https://conda.io/docs/user-guide/install/windows.html
            opts = [
                r"/InstallationType=JustMe",     # [JustMe|AllUsers]
                r"/AddToPath=1",                 # [0|1]—Default is 1
                r"/RegisterPython=0",            # 0 indicates JustMe, 1 indicates AllUsers.
                r"/S",                           # Silent mode
                r"/D=%UserProfile%\Miniconda3",  # Destination
                ]
            tl_call.doCallOrRaise([condaFil] + opts)
            print()
            print("===")
            print("You may want to add '%s' to your PATH" % os.path.join(condaDir, "bin"))
            print("and restart the scripts with conda python")
            print("===")
            print()
            newPATH = os.path.join(condaDir, "bin")
            newPATH = ';'.join( [newPATH, os.environ["PATH"]] )
            os.putenv("PATH", newPATH)

    if has_conda():
        if is_conda_system():
            LOGGER.warning("Conda is detected as system wide install.")
            LOGGER.warning("You may want to run the following commands in an elevated conda prompt:")
            LOGGER.warning("   conda update --all")
            for pkg in CONDA_PKGS['base']:
                LOGGER.warning("   conda install %s", ' '.join(pkg))

            grp = 'h2d2' if ctx.isH2D2() else 'tools'
            for pkg in CONDA_PKGS[grp]:
                LOGGER.warning("   conda install %s", ' '.join(pkg))
        else:
            LOGGER.info("Update conda")
            tl_call.doCallOrRaise(["conda", "update",  "--yes","--all"])

            for pkg in CONDA_PKGS['base']:
                LOGGER.info("Install %s", pkg)
                tl_call.doCallOrRaise(["conda", "install", "--yes"] + pkg)

            grp = 'h2d2' if ctx.isH2D2() else 'tools'
            for pkg in CONDA_PKGS[grp]:
                LOGGER.info("Install %s", pkg)
                tl_call.doCallOrRaise(["conda", "install", "--yes"] + pkg)

    if not has_conda():
        LOGGER.critical("scripts must be lauched with conda python")
        exit(1)

def xeq(ctx):
    LOGGER.trace(".".join([os.path.splitext(__file__)[0], "xeq"]))

    # ---  Header
    LOGGER.info("H2D2 - Conda")

    # ---  Pre-conditions
    assert ctx.target, "ctx.target must be defined"
    assert ctx.optDir, "ctx.optDir must be defined"
    assert ctx.dwnDir, "ctx.dwnDir must be defined"

    # ---  Install packages
    if tl_ptf.isWindows():
        installWin(ctx)
    else:
        installUnx(ctx)

    # ---  Check for GDAL_DATA
    if tl_ptf.isWindows():
        try:
            d_ = os.environ['GDAL_DATA']
        except KeyError:
            print()
            print("===")
            print("Environment variable GDAL_DATA not (yet) defined")
            print("You need open a new conda prompt to run the applications")
            print("===")
            print()
        
    # ---  Footer
    LOGGER.info("H2D2 - Conda: Done")

if __name__ == "__main__":
    from tools.context     import getTestContext
    from tools.addLogLevel import addLoggingLevel
    addLoggingLevel('DUMP',  logging.DEBUG + 5)
    addLoggingLevel('TRACE', logging.DEBUG - 5)

    logHndlr = logging.StreamHandler()
    FORMAT = "%(asctime)s %(levelname)s %(message)s"
    logHndlr.setFormatter( logging.Formatter(FORMAT) )

    LOGGER = logging.getLogger("H2D2.install")
    LOGGER.addHandler(logHndlr)
    LOGGER.setLevel(logging.DEBUG)
    LOGGER.info("unit test: %s", __file__)

    ctx = getTestContext()
    xeq(ctx)
